# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include scribus::install
class scribus::install {
  package { $scribus::package_name:
    ensure => $scribus::package_ensure,
  }
}
